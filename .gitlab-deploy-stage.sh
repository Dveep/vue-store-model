set -f
string=$STAGE_DEPLOY_SERVER
array=(${string//,/ })

for i in "${!array[@]}"; do
    echo "*** Deploying on server ${array[i]}... ***"
    ssh ubuntu@${array[i]} 'cd /var/www/vue-store-model/ && sudo git pull origin main'
done